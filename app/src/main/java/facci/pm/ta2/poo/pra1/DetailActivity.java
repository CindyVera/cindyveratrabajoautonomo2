package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    private View mProgressView1;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mProgressView1 = findViewById(R.id.progress);




        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

        // Se accede al objeto obtenido como parametro
        final String object_id = getIntent().getStringExtra("object_id");

        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id , new GetCallback<DataObject>() {
                    @Override

                    //metodo done() para acceder a las propiedades de la DataBase
                    public void done( DataObject object, DataException e) {

                        if (e == null) {

                            // se insertan los titulos precios y detalles de cada producto

                            TextView title = (TextView) findViewById(R.id.title);
                            title.setText((String) object.get("name"));

                            ImageView thumbnail = (ImageView) findViewById(R.id.thumbnail);
                            thumbnail.setImageBitmap((Bitmap) object.get("image"));

                            TextView price = (TextView) findViewById(R.id.price);
                            price.setText((String) object.get("price"));

                            TextView description = (TextView) findViewById(R.id.description);
                            description.setText((String) object.get("description"));

                            //Para el scroll de movimiento en la descripcion
                            description.setMovementMethod(new ScrollingMovementMethod());
                        } else {
                            // Error
                        }
                    }
                });
        // FIN - CODE6

    }

}
